<?php
/**
 * Template Name: Secondary Page
 * Description: Page template for any top level page. Has access to modules.
 */

$context = Timber::get_context();
$post = new Timber\Post();
$context['post'] = $post;

$today = date('Ymd');
$todayTime = date('Y-m-d H:i:s');

//GRABS MOST RECENT POSTS
$articleargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '4', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['posts'] = Timber::get_posts( $articleargs );

//GRABS MOST RECENT BLOG POSTS
$blogargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '6', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date'
    );
$context['blog'] = Timber::get_posts( $blogargs );


Timber::render( array( 'templates/landing-page.twig', 'page.twig' ), $context );
