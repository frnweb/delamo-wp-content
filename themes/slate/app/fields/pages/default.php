<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
  'ui' => 1,
  'wrapper' => ['width' => 100],
];

$default = new FieldsBuilder('page_settings', ['position' => 'acf_after_title']);

$default
  ->setLocation('post_type', '==', 'page')
    ->and('page_template', '!=', 'template-landing.php')
    ->and('page_template', '!=', 'template-formatting.php');
  
$default
  ->addTab('page_title', ['placement' => 'left'])
      ->addGroup('page_settings', [
        'label' => 'Card Content'
      ])
        // Intro Paragraph
        ->addTrueFalse('add_intro', [
          'label' => 'Add Intro Paragraph',
        ])
        ->setInstructions('Optional intro paragraph under page title')
        ->addWysiwyg('intro', [
          'label' => 'Intro Text',
        ])
      ->conditional('add_intro', '==', 1 )

      //Button
      ->addFields(get_field_partial('modules.button'))
      ->endGroup();

$default
  ->addTab('sidebar', ['placement' => 'left'])

      // Intro Paragraph
      ->addTrueFalse('sidebar', [
        'label' => 'Sidebar On',
      ]);
$default
  ->addTab('google_map', ['placement' => 'left'])

      // Intro Paragraph
      ->addTrueFalse('map', [
        'label' => 'Show Location Map Above The Footer',
      ]);

return $default;