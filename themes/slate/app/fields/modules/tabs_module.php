<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
    'ui' => 1,
    'wrapper' => ['width' => 50],
];

$tabs_module = new FieldsBuilder('tabs_module');

$tabs_module
    ->addTab('settings', ['placement' => 'left'])
        ->addFields(get_field_partial('partials.add_class'))
        ->addFields(get_field_partial('partials.module_title'));


$tabs_module
    ->addTab('content', ['placement' => 'left'])

    //Header
    ->addText('header', [
        'label' => 'Tab Header'
        ])
        ->setInstructions('This is optional')

    //Repeater
    ->addRepeater('tabs', [
      'min' => 1,
      'max' => 12,
      'button_label' => 'Add Tab',
      'layout' => 'block',
      'wrapper' => [
          'class' => 'deck',
        ],
    ])

    ->addFields(get_field_partial('partials.add_class'))

    //Tab Title
    ->addText('title', [
        'label' => 'Tab Title'
        ])

    ->addFlexibleContent('pieces', ['button_label' => 'Add Pieces'])

        // WYSIWYG
        ->addLayout('wysiwyg')
            ->addWysiwyg('paragraph', [
                'label' => 'Wysiwyg',
            ])

        // DUO
        ->addLayout('duo')
            //Image 
            ->addImage('duo_image')
            //WYSIWYG
            ->addWysiwyg('duo_content', [
                'label' => 'Wysiwyg',
            ])
            //Buttons
            ->addFields(get_field_partial('modules.button'))

        //DECK
        ->addLayout('deck')
            //Repeater
            ->addRepeater('deck', [
              'min' => 1,
              'max' => 12,
              'button_label' => 'Add Card',
              'layout' => 'block',
              'wrapper' => [
                  'class' => 'deck',
                ],
            ])

            ->addFields(get_field_partial('partials.add_class'))
            ->addFields(get_field_partial('partials.wildcard_grid'))

            //Image 
            ->addGroup('card_image', [
                'label' => 'Card Image'
            ])
                ->addTrueFalse('add_image', [
                    'label' => 'Add Image to Card',
                    'wrapper' => ['width' => 15]
                    ])
                    ->addImage('image', [
                        'wrapper' => ['width' => 50]
                    ])
                    ->conditional('add_image', '==', 1)
            ->endGroup()

            // Card
            ->addFields(get_field_partial('modules.card'));


return $tabs_module;
        