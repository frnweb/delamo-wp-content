<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$config = (object) [
	'ui' => 1,
	'wrapper' => ['width' => 30],
];

$testimonial = new FieldsBuilder('testimonial');

$testimonial
	->addTab('settings', ['placement' => 'left'])
		->addFields(get_field_partial('partials.add_class'))
		->addFields(get_field_partial('partials.module_title'));

$testimonial
	->addTab('content', ['placement' => 'left'])

		// Header
		->addText('header', [
			'label' => 'Module Title',
			'ui' => $config->ui
		])
		
    	//Repeater
		->addRepeater('testimonials', [
		  'min' => 1,
		  'max' => 10,
		  'button_label' => 'Add Item',
		  'layout' => 'block',
		])

		//Image 
		->addImage('image')

		// Quote
		->addWysiwyg('quote', [
			'label' => 'Quote',
			'ui' => $config->ui
		])

		// Cite
		->addText('cite', [
			'label' => 'Cite',
			'ui' => $config->ui
		]);

return $testimonial;