<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

$articleargs = array(
    'post_type'      => 'post',
    'posts_per_page' => '9', // Number of posts
    'order'          => 'DESC',
    'orderby'        => 'date'
    );

$context = Timber::get_context();
$context['posts'] = new Timber\PostQuery( $articleargs);

$context['categories'] = Timber::get_terms('category');

$templates = array( 'index.twig' );
if ( is_home() ) {
	array_unshift( $templates, 'home.twig' );
}

Timber::render( $templates, $context );
