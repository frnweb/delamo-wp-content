<?php
/**
 * Plugin Name: Slate Plugin
 * Description: A plugin to optimize and customize the wordpress backend for the Slate theme 
 **/
 
require_once( 'slate-plugin/slate-plugin.php' );