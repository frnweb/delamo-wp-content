<?php
/**
 * Plugin Name: Timber Library
 * Description: This plugin installs the Timber used on most of our sites. 
 **/
 
require_once( 'timber-library/timber.php' );