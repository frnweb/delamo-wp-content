<?php
//READ NEXT
	function frn_read_next ( $atts, $content = null ) {
		$specs = shortcode_atts( array(
			'url'	=> '#',
			'title' => '',
			'style'	=> '', 
			'target'	=> '_self'
			), $atts );

			return '<div class="sl_read-next ' . esc_attr($specs['style'] ) . ' "><span>Read This Next:</span> <a class="sl_read-next--link" title="' . esc_attr($specs['title'] ) . '" href="' . esc_attr($specs['url'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a></div>';
	}

	add_shortcode ('read-next', 'frn_read_next' );
?>