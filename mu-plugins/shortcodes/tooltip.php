<?php
	// TOOLTIP
		function sl_tooltip ( $atts, $content = null ) {
			$specs = shortcode_atts( array(
				'definition'	=> '',
				), $atts );

			return '<span data-tooltip data-position="bottom" data-alignment="center" data-tooltip-class="sl_tooltip" class="sl_tooltip__word" title="' . esc_attr($specs['definition'] ) . '">' . $content . '</span>';
			};

		add_shortcode ('tooltip', 'sl_tooltip' );
	///TOOLTIP
?>